package org.prior.game.scopevariable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScopeVariableApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScopeVariableApplication.class, args);
	}

}
