# JAVA-27
Scope variable
# database
How to use it

1. Download and install 
    https://www.docker.com/products/docker-desktop

2. Pull image mysql using command
    docker pull mysql:latest

3. Open terminal and go to JAVA-27 folder

4. Start docker database using command
    docker-compose up -d

5. If you would like to stop docker database using command
    docker-compose down -v

# Postman
https://www.getpostman.com/collections/4c0e437a752516f9e9b8

# Connect from database tool
MYSQL_DATABASE: prior_game_db
MYSQL_USER: dev
MYSQL_PASSWORD: dev